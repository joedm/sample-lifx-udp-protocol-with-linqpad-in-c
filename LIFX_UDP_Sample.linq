<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>System.Net.Sockets</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Globalization</Namespace>
</Query>

void Main()
{	
	UdpClient udpClient = new UdpClient();
	udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, 56700));
	var from = new IPEndPoint(0, 0);
	
	var macAddresses = GetAllMacAddressOnNetwork(udpClient, from, 4);

	var targetMacAddress = macAddresses.OrderBy(x => Guid.NewGuid()).First(); // get one at random
	// var targetMacAddress = new byte[] { 0xd0, 0x73, 0xd5, 0x3b, 0x00, 0x00 }; // If you know the exact mac address
	bool isOn = GetPower(udpClient, from, targetMacAddress);
	SetPower(udpClient, from, !isOn, targetMacAddress);

	udpClient.Dispose();
}

List<byte[]> GetAllMacAddressOnNetwork(UdpClient udpClient, IPEndPoint from, int devicesOnNetwork)
{
	Console.WriteLine($"Getting {devicesOnNetwork} mac addresses on the network.");
	var getDevices = new byte[]
	{
		// Header
		0x24, 0x00, // size (number of total bytes in packet including these two)
		0x00, 0x34, // origin, tagged, addressable and protocol (remember to use little endian, original binary would be 00110100 00000000)
		0x00, 0x00, 0x00, 0xaa, // source
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // target (all 0's for all)
		0x4c, 0x49, 0x46, 0x58, 0x56, 0x32, // reserved (doco says must be all 0's but we insert "LIFXV2")
		0x03, // ack required and response required ()
		0x00, 0x00, // sequence
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // reserved
		0x02, 0x00, // message type, we use 2 for get device 
		0x00, 0x00 // reserved
	};
	
	udpClient.Send(getDevices, getDevices.Length, "255.255.255.255", 56700);

	int repliesExpected = (devicesOnNetwork * 3) + 1;
	List<byte[]> allMacAddressesInBroadcastCaptured = new List<byte[]>();
	for (int i = 0; i < repliesExpected; i++) // my broadcast plus number of devices (an ack and response from each)
	{
		var recvBuffer = udpClient.Receive(ref from);
		allMacAddressesInBroadcastCaptured.Add(recvBuffer.Skip(8).Take(6).ToArray());
		Console.WriteLine(BitConverter.ToString(recvBuffer));
	}
	
	Console.WriteLine("--------------------");
	return allMacAddressesInBroadcastCaptured.Where(x => !x.All(y => y == 0x00)).Distinct().ToList();
}

bool GetPower(UdpClient udpClient, IPEndPoint from, byte[] macAddress) 
{
	Console.WriteLine($"Getting power status of MAC Address: {BitConverter.ToString(macAddress)}");
	var getPowerPacket = new byte[]
	{
		// Header
		0x24, 0x00, // size (number of total bytes in packet including these two)
		0x00, 0x14, // origin, tagged, addressable and protocol (with tagged = 0) (original binary would be 00010100 00000000)
		0x00, 0x00, 0x00, 0xaa, // source
		macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5], 0x00, 0x00, // target
		0x4c, 0x49, 0x46, 0x58, 0x56, 0x32, // reserved (doco says must be all 0's but we insert "LIFXV2")
		0x03, // ack required and response required ()
		0x00, 0x00, // sequence
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // reserved
		0x14, 0x00, // message type, we use 20 for get power
		0x00, 0x00, // reserved
	};

	udpClient.Send(getPowerPacket, getPowerPacket.Length, "255.255.255.255", 56700);

	bool isOn = false;
	for (int i = 0; i < 3; i++) // my broadcast plus however many responses we expect to receive
	{
		var recvBuffer = udpClient.Receive(ref from);
		Console.WriteLine(BitConverter.ToString(recvBuffer));
		isOn = isOn || recvBuffer.Last() == 0xFF; // Checking just the last byte works as a proof of concept. Ideally we'd make sure it's the right response (message type 0x16 in HEX) and check for any value greater than 0x00, 0x00 in last two bytes.
	}
	
	Console.WriteLine("--------------------");
	return isOn;
}

void SetPower(UdpClient udpClient, IPEndPoint from, bool on, byte[] macAddress)
{
	Console.WriteLine($"Setting power status of MAC Address: {BitConverter.ToString(macAddress)} to {(on ? "on" : "off")}");
	byte powerByte = on ? (byte)0xFF : (byte)0x00;
	var setPowerPacket = new byte[]
	{
		// Header
		0x26, 0x00, // size (number of total bytes in packet including these two)
		0x00, 0x14, // origin, tagged, addressable and protocol (with tagged = 0) (original binary would be 00010100 00000000)
		0x00, 0x00, 0x00, 0xaa, // source
		macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5], 0x00, 0x00, // target
		0x4c, 0x49, 0x46, 0x58, 0x56, 0x32, // reserved (doco says must be all 0's but we insert "LIFXV2")
		0x00, // ack required and response required ()
		0x00, 0x00, // sequence
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // reserved
		0x15, 0x00, // message type, we use 21 for set power
		0x00, 0x00, // reserved
		
		// Payload
		powerByte, powerByte // power level 0 - 65535
	};

	udpClient.Send(setPowerPacket, setPowerPacket.Length, "255.255.255.255", 56700);

	for (int i = 0; i < 1; i++) // my broadcast plus no reply because we set ack_require and res_required to false. If this were set to true, the response will contain the power status before processing our request.
	{
		var recvBuffer = udpClient.Receive(ref from);
		Console.WriteLine(BitConverter.ToString(recvBuffer));
	}
	
	Console.WriteLine("--------------------");
}
